import torch
import math
import torchvision
import torch.nn as nn
import numpy as np
import torch.nn.functional as F


class ImgNet(nn.Module):
    def __init__(self, code_len):
        super(ImgNet, self).__init__()
        self.alexnet = torchvision.models.alexnet(pretrained=True)
        self.alexnet.classifier = nn.Sequential(*list(self.alexnet.classifier.children())[:6])
        self.hash_layer = nn.Linear(4096, code_len)
        self.alpha = 1.0

    def forward(self, x):

        with torch.no_grad():
            x = self.alexnet.features(x)
            x = x.view(x.size(0), -1)
            feat = self.alexnet.classifier(x)

        hid = self.hash_layer(feat)
        feat = F.normalize(feat, dim=1)
        code = torch.tanh(self.alpha * hid)

        return feat, hid, code

    def set_alpha(self, epoch):
        self.alpha = math.pow((1.0 * epoch + 1.0), 0.5)


class TxtNet(nn.Module):
    def __init__(self, code_len, txt_feat_len):
        super(TxtNet, self).__init__()

        self.net = nn.Sequential(nn.Linear(txt_feat_len, 4096),
                                 nn.ReLU(inplace=True),
                                 nn.Linear(4096, code_len),
                                 )

        self.alpha = 1.0

    def forward(self, x):
        hid = self.net(x)
        code = torch.tanh(self.alpha * hid)
        return code

    def set_alpha(self, epoch):
        self.alpha = math.pow((1.0 * epoch + 1.0), 0.5)


class Patch_embeded(nn.Module):
    def __init__(self, image_size=224,text_size=224,num=5000,txt_dim=100, patch_size=16, embed_dim=768, in_channel_img=3,in_channel_txt=1):
        super().__init__()
        self.patch_size = patch_size
        self.embed_dim = embed_dim
        self.in_channel_img = in_channel_img
        self.in_channel_txt=in_channel_txt
        self.image_size = image_size
        self.text_size=text_size
        self.token_type_embeddings = nn.Embedding(num, txt_dim)
        self.num_patches_img = (self.image_size // self.patch_size) ** 2
        self.num_patches_txt=self.text_size // self.patch_size
        self.proj_img = nn.Conv2d(in_channels=self.in_channel_img, out_channels=self.embed_dim, kernel_size=self.patch_size, stride=self.patch_size)
        self.proj_txt=nn.Conv1d(in_channels=self.in_channel_txt,out_channels=self.embed_dim,kernel_size=self.patch_size,stride=self.patch_size)

        self.similarity_token = nn.Parameter(torch.zeros(1, 1, self.embed_dim))
        self.pos_embed_img = nn.Parameter(torch.zeros(1, self.num_patches_img , self.embed_dim))
        self.pos_embed_txt=nn.Parameter(torch.zeros(32,86,self.embed_dim))

    def forward(self, x):
        # x: [B, C, H, W]
        img,txt=x  # img: [B,C,H,W]   txt:[B.txt_len]

        img = self.proj_img(img)  # [B, embed_dim, num_patches]

        img=img.flatten(2)
        #
        txt=txt.unsqueeze(1)


        txt=self.proj_txt(txt)
        #print("txt", txt.shape)
        img = img.transpose(1, 2)  # [B, num_patches, embed_dim]
        txt=txt.transpose(1,2)
        img=img+torch.ones(img.shape).to("cuda") # type_embedding

        #txt=txt+torch.ones_like(torch.tensor(txt.shape) ).to("cuda")
        #x=torch.cat((img,txt),dim=1)

        similarity_token = self.similarity_token.expand(img.shape[0], -1, -1)

        pos_embed_img = self.pos_embed_img
        pos_embed_txt=self.pos_embed_txt
        img = img + pos_embed_img  # [B, num_patches+1, embed_dim]
        txt=txt+pos_embed_txt
        x=torch.cat((img,txt),dim=1)

        out = torch.cat((x, similarity_token), dim=1)  # [B, num_patches+1, embed_dim]


        #token_type_embeddings=self.token_type_embeddings

        return out




class attention(nn.Module):
    def __init__(self, dim, num_heads=8, qkv_bias=False):
        super().__init__()

        self.num_heads = num_heads
        prehead_dim = dim // self.num_heads
        self.scale = prehead_dim ** -0.5

        self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
        self.proj = nn.Linear(dim, dim)

    def forward(self, x):
        # x: [B, num_patches+1, embed_dim]
        B, num_patches, total_dim = x.shape

        qkv = self.qkv(x)  # [B, num_patches+1, 3*embed_dim]

        qkv = qkv.reshape(B, num_patches, 3, self.num_heads,
                          total_dim // self.num_heads)  # [B, num_patches+1, 3, num_heads, prehead_dim]

        qkv = qkv.permute(2, 0, 3, 1, 4)  # [3, B, num_heads, num_patches+1, prehead_dim]
        q, k, v = qkv[0], qkv[1], qkv[2]  # [B, num_heads, num_patches+1, prehead_dim]

        atten = (q @ k.transpose(-2, -1)) * self.scale  # [B, num_heads, num_patches+1, num_patches+1]
        atten = atten.softmax(dim=-1)
        atten = atten @ v  ## [B, num_heads, num_patches+1, prehead_dim]
        atten = atten.transpose(1, 2)  # [B, num_patches+1, num_heads, prehead_dim]
        atten = atten.reshape(B, num_patches, total_dim)  # [B, num_patches+1, embed_dim]

        out = self.proj(atten)

        return out


class MLP(nn.Module):
    def __init__(self, in_dim, hidden_dim=None, out_dim=None):
        super().__init__()
        self.fc1 = nn.Linear(in_dim, hidden_dim)
        self.actlayer = nn.GELU()
        self.fc2 = nn.Linear(hidden_dim, out_dim)

    def forward(self, x):
        x = self.fc1(x)  # [B, num_patches+1, hidden_dim]
        x = self.actlayer(x)
        x = self.fc2(x)  # [B, num_patches+1, out_dim]
        x = self.actlayer(x)

        return x


class Encoder_block(nn.Module):
    def __init__(self,
                 dim,
                 num_heads,
                 mlp_ration=4,
                 qkv_bias=False,

                 ):
        super().__init__()

        self.normlayer = nn.LayerNorm(dim)
        self.atten = attention(dim, num_heads, qkv_bias=qkv_bias)
        self.hidden_dim = int(dim * mlp_ration)
        self.mlp = MLP(in_dim=dim, hidden_dim=self.hidden_dim, out_dim=dim)

    def forward(self, x):
        x = x + self.atten(self.normlayer(x))
        x = x + self.mlp(self.normlayer(x))

        return x


class Transformer(nn.Module):
    def __init__(self,
                 in_channel=1,
                 dim=32,
                 num_heads=12,
                 image_size=224,
                 patch_szie=16,
                 num_classes=2,
                 depth=12,
                 qkv_bias=True
                 ):
        super().__init__()
        self.image_size = image_size
        self.patch_size = patch_szie
        self.patch_embed = Patch_embeded(image_size=224,text_size=224,num=5000,txt_dim=100, patch_size=16, embed_dim=768, in_channel_img=3,in_channel_txt=1)
        self.depth = depth
        self.norm = nn.LayerNorm(dim)
        self.encoder = nn.Sequential(*[
            Encoder_block(dim=dim, num_heads=num_heads, mlp_ration=4, qkv_bias=qkv_bias) for i in range(depth)
        ])

        self.head = nn.Linear(dim, num_classes)

    def forward(self, x):
        img,txt=x
        x = self.patch_embed((img,txt))
        x = self.encoder(x)
        x = self.norm(x)
        x = self.head(x)

        return x[:,0]
